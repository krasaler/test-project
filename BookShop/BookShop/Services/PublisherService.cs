﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookShop.Services
{
    public class PublisherService
    {
        public static List<Publisher> GetAll()
        {
            using (var context = new BookShopEntities())
            {
                return context.Publishers.Include(s => s.Books).ToList();
            }
        }
        public static Publisher Get(int publisherId)
        {
            using (var context = new BookShopEntities())
            {
                return context.Publishers.Include(s => s.Books).FirstOrDefault(s => (s.Id == publisherId));
            }
        }

        public static void Save(Publisher publisher)
        {
            using (var context = new BookShopEntities())
            {
                context.Publishers.Attach(publisher);
                context.Entry(publisher).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public static void Create(Publisher publisher)
        {
            using (var context = new BookShopEntities())
            {
                context.Publishers.Add(publisher);
                context.SaveChanges();
            }
        }
        public static void Delete(Publisher publisher)
        {
            using (var context = new BookShopEntities())
            {
                context.Entry(publisher).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}