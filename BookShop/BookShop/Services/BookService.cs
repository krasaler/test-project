﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookShop.Services
{
    public class BookService
    {
        public static List<Book> GetAll()
        {
            using (var context = new BookShopEntities())
            {
                return context.Books.Include(s => s.Publishers).Include(s=>s.Authors).ToList();
            }
        }
        public static Book Get(int bookId)
        {
            using (var context = new BookShopEntities())
            {
                return context.Books.Include(s => s.Publishers).Include(s => s.Authors)
                    .FirstOrDefault(s => (s.Id == bookId));
            }
        }

        public static void Save(Book book)
        {
            using (var context = new BookShopEntities())
            {   
                context.Books.Attach(book);
                context.Entry(book).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public static void Create(Book book)
        {
             using (var context = new BookShopEntities())
             {
                context.Books.Add(book);
                context.SaveChanges();
             }
        }
        public static void Delete(Book book)
        {
            using (var context = new BookShopEntities())
            {
                context.Entry(book).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}