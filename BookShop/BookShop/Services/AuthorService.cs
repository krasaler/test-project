﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookShop.Services
{
    public class AuthorService
    {
        public static List<Author> GetAll()
        {
            using (var context = new BookShopEntities())
            {
                return context.Authors.Include(s=>s.Books).ToList();
            }
        }
        public static Author Get(int authorId)
        {
            using (var context = new BookShopEntities())
            {
                return context.Authors.Include(s => s.Books).FirstOrDefault(s => (s.Id == authorId));
            }
        }

        public static void Save(Author author)
        {
            using (var context = new BookShopEntities())
            {
                context.Authors.Attach(author);
                context.Entry(author).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
        public static void Create(Author author)
        {
            using (var context = new BookShopEntities())
            {
                context.Authors.Add(author);
                context.SaveChanges();
            }
        }
        public static void Delete(Author author)
        {
            using (var context = new BookShopEntities())
            {
                context.Entry(author).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }
    }
}