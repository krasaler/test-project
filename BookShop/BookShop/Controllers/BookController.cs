﻿using BookShop.Services;
using BookShop.ViewModelHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookShop.ViewModels;

namespace BookShop.Controllers
{
    public class BookController : Controller
    {
        // GET: Book
        public ActionResult Index()
        {
            var books = BookViewModelHelper.PopulateBookViewModelHelperList(BookService.GetAll());
            return View(books);
        }

        public ActionResult Create()
        {
            HtmlHelper.ClientValidationEnabled = true;
            HtmlHelper.UnobtrusiveJavaScriptEnabled = true;
            var model = new BookEditViewModel();
            var authors = AuthorService.GetAll();
            BookEditViewModelHelper.PopulateAuthorCheckList(model, authors);
            var publishers = PublisherService.GetAll();
            BookEditViewModelHelper.PopulatePublisherList(model, publishers);
            model.PublishedAt = DateTime.Now;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(BookEditViewModel model)
        {
            TempData["model"] = model;
            return RedirectToAction("Confirm");
        }

        public ActionResult Details(int bookId)
        {
            var book = BookService.Get(bookId);
            if (book != null)
            {
                var model = BookEditViewModelHelper.PopulateBookEditViewModel(book);
                var authors = AuthorService.GetAll();
                BookEditViewModelHelper.PopulateAuthorCheckList(model, authors);
                var publishers = PublisherService.GetAll();
                BookEditViewModelHelper.PopulatePublisherList(model, publishers);
                return View(model);
            }
            return RedirectToRoute("Index", "Book");
        }
        
        public ActionResult Confirm()
        {
            var model = (BookEditViewModel)TempData["model"];
            var publishers = PublisherService.GetAll();
            BookEditViewModelHelper.PopulatePublisherList(model, publishers);
            return View(model);
        }
        [HttpPost]
        public ActionResult Confirm(BookEditViewModel model)
        {
            Book book = BookEditViewModelHelper.PopulateBookFromEditViewModel(model);
            BookService.Create(book);
            return RedirectToAction("Index");
        }
    }
}