﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookShop.ViewModelHelpers;
using BookShop.ViewModels;
using BookShop.Services;

namespace BookShop.Controllers
{
    public class AuthorController : Controller
    {
        // GET: Author
        public ActionResult Create()
        {
            return View(AuthorViewModelHelper.PopulateAuthorViewModelHelper(new Author()));
        }

        [HttpPost]
        public ActionResult Create(AuthorViewModel model)
        {
            var author = AuthorViewModelHelper.PopulateAuthorFromEditViewModel(model);
            if (author != null)
            {
                AuthorService.Create(author);
            }
            return RedirectToAction("Index", "Book");
        }
    }
}