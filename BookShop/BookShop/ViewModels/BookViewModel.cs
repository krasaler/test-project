﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookShop.ViewModels
{
    public class BookViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Publisher")]
        public Publisher Publisher { get; set; }

        [Display(Name = "Price")]
        public double Price { get; set; }

        [Display(Name = "Date Published")]
        public System.DateTime PublishedAt { get; set; }

        public List<Author> Authors { get; set; }

    }
}