﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookShop.ViewModels
{
    public class AuthorViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The field {0} is required")]
        [Display(Name="Name")]
        public string Name { get; set; }

        public List<Book> Books { get; set; }
    }
}