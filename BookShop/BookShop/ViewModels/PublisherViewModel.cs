﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookShop.ViewModels
{
    public class PublisherViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Поле {0} обязательно для заполнения")]
        [Display(Name = "Название")]
        public string Name { get; set; }

        public List<Book> Books { get; set; }
    }
}