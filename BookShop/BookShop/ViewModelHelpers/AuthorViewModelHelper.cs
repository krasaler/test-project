﻿using BookShop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookShop.ViewModelHelpers
{
    public class AuthorViewModelHelper
    {
        public static AuthorViewModel PopulateAuthorViewModelHelper(Author author)
        {
            return new AuthorViewModel
            {
                Id = author.Id,
                Name = author.Name,
                Books = author.Books.ToList()
            };
        }
        public static Author PopulateAuthorFromEditViewModel(AuthorViewModel model)
        {
            return new Author
            {
                Id = model.Id,
                Name = model.Name
            };
        }
        public static List<AuthorViewModel> PopulateAuthorViewModelHelperList(List<Author> authorList)
        {
            return authorList.Select(s => PopulateAuthorViewModelHelper(s)).ToList();
        }
    }
}