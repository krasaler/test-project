﻿using BookShop.Services;
using BookShop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookShop.ViewModelHelpers
{
    public class BookViewModelHelper
    {
        public static BookViewModel PopulateBookViewModelHelper(Book book)
        {
            return new BookViewModel
            {
                Id = book.Id,
                Name = book.Name,
                Description = book.Description,
                Price = book.Price,
                Publisher = PublisherService.Get(book.IdPublisher),
                PublishedAt = book.PublishedAt,
                Authors = book.Authors.ToList()
            };
        }

        public static List<BookViewModel> PopulateBookViewModelHelperList(List<Book> bookList)
        {
            return bookList.Select(s => PopulateBookViewModelHelper(s)).ToList();
        }
    }
}