﻿using BookShop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookShop.ViewModelHelpers
{
    public class PublisherViewModelhelper
    {
        public static PublisherViewModel PopulatePublisherViewModelHelper(Publisher publisher)
        {
            return new PublisherViewModel
            {
                Id = publisher.Id,
                Name = publisher.Name,
                Books = publisher.Books.ToList()
            };
        }

        public static Publisher PopulatePublisherFromEditViewModel(PublisherViewModel model)
        {
            return new Publisher
            {
                Id = model.Id,
                Name = model.Name
            };
        }

        public static List<PublisherViewModel> PopulatePublisherViewModelHelperList(List<Publisher> publisherList)
        {
            return publisherList.Select(s => PopulatePublisherViewModelHelper(s)).ToList();
        }
    }
}